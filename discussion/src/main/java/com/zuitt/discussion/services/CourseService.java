package com.zuitt.discussion.services;


import com.zuitt.discussion.models.Course;
import org.springframework.http.ResponseEntity;

public interface CourseService {

    //    Create a course
    void createCourse(String stringToken, Course course);

    //    Viewing all courses
    Iterable<Course> getCourses();

    //    Delete a course
    ResponseEntity deleteCourse(int id, String stringToken);

    //    Update a course
    ResponseEntity updateCourse(int id, String stringToken, Course course);


}

